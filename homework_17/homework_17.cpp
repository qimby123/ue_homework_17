﻿// homework_17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Matrix {

    double* els;
    int size;

public:
    Matrix() {
        els = new double[3 * 3]{ 0 };
        size = 3;
    }

    Matrix(int size) {
        els = new double[size * size]{ 0 };
        this->size = size;
    }

    double getSize() {
        return size;
    }

    double getElement(int row, int col) {
        
        int index = row * size + col;
        if ( index >= size*size || row < 0 || col < 0 ) {
            std::cout << "\nrow or col out of range";
            return -1;
        }
        return els[index];
    }

    void setElement(int row, int col, double val) {
        int index = row * size + col;
        if ( index >= size * size || row < 0 || col < 0 ) {
            std::cout << "\nrow or col out of range";
            return;
        }
        els[index] = val;
    }

    void Print() {
        for (int i = 0; i < size * size; i++) {
            if (i % size == 0)
                std::cout << "\n";

            std::cout << els[i] << " ";
        }
    }
};

class Vector 
{
    double x;
    double y;
    double z;

public:
    Vector() : x{ 0 }, y{ 0 }, z{ 0 }
    {}

    Vector(double _x, double _y, double _z) : x{ _x }, y{ _y }, z{ _z }
    {}

    void Print()
    {
        std::cout << "\n" << x << " " << y << " " << z;
    }

    double Length() {
        return sqrt(x * x + y * y + z * z);
    }
};

int main()
{
    Matrix m;
    m.Print();
    m.getElement(0, 2);
    m.setElement(0, 2, 5);
    m.Print();

    Vector v(3, 5, -1);
    v.Print();
    std::cout<<"\n"<<v.Length();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
